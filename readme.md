Programa: Curso: Informe 1: 
Bachillerato en Ingeniería de software – Universidad Cenfotec BiSoft 32 – Calidad, verificación y validación de software Desarrollar programas contra dominio de problema y requerimientos funcionales abreviados Profesor: Emilio Venegas Página 1 de 2 

1. Características del calendario gregoriano 
Investigar en la Web o en otras fuentes (enciclopedias, etc.) lo siguiente: 
- ¿Qué es el calendario juliano? 
- ¿Qué es el calendario gregoriano? 
- ¿Cuándo fue instituido el calendario gregoriano? 
- ¿Cuáles deficiencias o limitaciones del calendario juliano viene a subsanar el calendario 
gregoriano? 
- Determinar la relación entre las fechas en que murieron los escritores Miguel de Cervantes y 
William Shakespeare. 

2. Requerimientos funcionales generales 
Con las propiedades del calendario gregoriano que Ud. investigó y el supuesto de que las fechas por 
tratar siempre están en un año posterior a 1582 transforme los siguientes requerimientos generales en 
requerimientos funcionales más precisos. Todos los requerimientos tienen como contexto el dominio de 
las fechas en el calendario gregoriano. 
- R0: Todas las fechas serán presentadas como tuplas de tres números enteros positivos (ternas), 
en este orden: (año, mes, día). 
- R1 (bisiesto): Dado un año, determinar si este es bisiesto. 
- R2 (fecha_es_valida): Dada una fecha, determinar si ésta es válida. 
- R3 (dia_siguiente): Dada una fecha válida, determinar la fecha del día siguiente. 
- R4 (dia_semana): Dada una fecha válida, determinar el día de la semana que le corresponde, 
con la siguiente codificación: 0 = domingo, 1 = lunes, 2 = martes, 3 = miércoles, 4 = jueves, 5 = 
viernes, 6 = sábado. 

3. Restricciones no funcionales y técnicas 
- Diseñe y programe funciones o métodos que implementen los requerimientos funcionales del 
apartado anterior. No es válido reutilizar funciones o métodos de bibliotecas. Toda 
programación por entregar debe ser producto de su propio trabajo. Todo cálculo y lógica del 
programa debe ser explicado, sea en comentarios o en documentación externa al programa. La 
documentación y los comentarios en el código deben hacerse en idioma español. 
- Ud. tiene libertad para extender los requerimientos en cuanto a la detección de casos de error y 
la manera en que estos serán señalados vía programación. 
- No nos interesa construir ni evaluar una interfaz de usuario amigable (en este momento). 
- Resuelva todos los requerimientos en lenguaje Java. 
- Cada requerimiento R1,..., R4 debe codificarse como un método aparte. 
- Presente un programa principal main con un llamado a cada uno de los métodos creados para 
los requerimientos R1, ...,R4, utilizando una tupla de ejemplo en cada caso y desplegar el 
resultado en la salida estandar. 
- Construya su programa de manera que el código sea legible según las características estudiadas 
en el tema de Revisiones Estáticas. 
- Construya los casos de unidad utilizando JUnit. Utilice las técnicas de Partición de equivalencias 
y Análisis de valores de frontera. 
Presente un cuadro o tabla donde se indiquen las equivalencias y valores de frontera utilizados. 
- Debe ser realizada por todos los miembros del proyecto. 
- Deben crearse una carpeta en Google drive para todo el proyecto, y dentro de ella una carpeta para cada entregable. En esta ocasión deben crear una carpeta con el entregable #1 y subir toda la documentación y código a la misma. 
- Su solución debe ser entregada en una carpeta comprimida, que comprenda: portada (que 
identifique a los miembros del grupo), requerimientos funcionales, decisiones de diseño tomadas, 
código fuente de su solución, evidencias de las pruebas realizadas (casos de unidad), análisis de 
resultados obtenidos. El código fuente debe estar en un archivo aparte. 
Fecha de entrega: domingo 13 de octubre de 2019, 23:59 
- Enviar su solución a la dirección del profesor: evenegas@ucenfotec.ac.cr 
- Cada solución tendrá como identificación “Informe 1”, concatenado con los números de carnet de 
los estudiantes, ordenados ascendentemente (de izquierda a derecha) y separados por un guion 
medio rodeado por espacios (' - '). 
- El asunto de su correo debe tener el prefijo “BISOFT32: “, seguido por el nombre completo del 
archivo como se indicó arriba. 
