package com.cenfotec.qagrupo5;

import org.junit.Test;

public class Main {

    public static void main(String[] args) throws Exception {

        Calendario calendario = new Calendario();
        try {
            int[] siguienteFecha = calendario.siguienteDia(new int[]{2019, 12, 31});

            System.out.println("Fecha es valida " + calendario.esFechaValida(new int[]{2019, 11, 3}));

            System.out.println("El dia de la semana es " + calendario.diaDeLaSemana(new int[]{2019, 10, 15}));

            System.out.println("Es annio bisiesto " + (calendario.esBisiesto(2019) ? " SI " : "NO"));

            System.out.println("Siguiente fecha valida " + siguienteFecha[0] + "-" + siguienteFecha[1] + "-" + siguienteFecha[2]);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
