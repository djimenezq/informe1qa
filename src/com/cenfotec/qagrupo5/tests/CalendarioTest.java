package com.cenfotec.qagrupo5.tests;

import com.cenfotec.qagrupo5.Calendario;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class CalendarioTest {

    private final Calendario calendario = new Calendario();

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void isLeapYearIfReturnTrue() {

        assertFalse("Anio 2015",calendario.esBisiesto(2015));
        assertTrue("Anio Bisiesto 2016",calendario.esBisiesto(2016));
        assertFalse("Anio 2017",calendario.esBisiesto(2017));
    }

    @Test
    public void dateShouldReturnNextDay() throws Exception {

        assertArrayEquals("Entre mes", new int[]{2019, 10, 30} , calendario.siguienteDia(new int[]{2019, 10, 29}));
        assertArrayEquals("Fin de mes de 30 dias", new int[]{2019, 10, 1} , calendario.siguienteDia(new int[]{2019, 9, 30}));
        assertArrayEquals("Fin de mes de 31 dias", new int[]{2019, 11, 1} , calendario.siguienteDia(new int[]{2019, 10, 31}));
        assertArrayEquals("Fin de mes de 28 dias", new int[]{2019, 3, 1} , calendario.siguienteDia(new int[]{2019, 2, 28}));
        assertArrayEquals("Fin de febrero en anio bisiesto", new int[]{2016, 3, 1} , calendario.siguienteDia(new int[]{2016, 2, 29}));
        assertArrayEquals("Fin de anio", new int[]{2020, 1, 1} , calendario.siguienteDia(new int[]{2019, 12, 31}));
    }

    @Test
    public void expectedExceptionIfDateIsInvalid() throws Exception {

        exceptionRule.expect(Exception.class);
        exceptionRule.expectMessage("Fecha Invalida");
        assertArrayEquals("Excepcion espereda para dia siguiente", new int[]{2019, 3, 1}, calendario.siguienteDia(new int[]{2019, 2, 29}));
        assertEquals("Excepcion espereda para numero de dia", 5 , calendario.diaDeLaSemana(new int[]{2019, 2, 29}));
    }

    @Test
    public void isValidDateIfReturnTrue(){
        assertFalse("Fecha antes de 1582", calendario.esFechaValida(new int[]{1581, 1, 1}));
        assertTrue("Fecha igual a 1582", calendario.esFechaValida(new int[]{1582, 1, 1}));
        assertTrue("Fecha despues de 1582", calendario.esFechaValida(new int[]{1583, 1, 1}));
        assertFalse("Mes invalido", calendario.esFechaValida(new int[]{2019, 13, 1}));
        assertFalse("Dia invalido en mes de 31 dias", calendario.esFechaValida(new int[]{2019, 12, 32}));
        assertFalse("Dia invalido en mes de 30 dias", calendario.esFechaValida(new int[]{2019, 9, 31}));
        assertTrue("Anio bisiesto valido", calendario.esFechaValida(new int[]{2016, 2, 29}));
        assertFalse("Anio bisiesto invalido", calendario.esFechaValida(new int[]{2019, 2, 29}));
    }

    @Test
    public void dateShouldReturnNumberOfDay()throws Exception {

        assertEquals("Dia 0", 0,  calendario.diaDeLaSemana(new int[]{2019, 10, 13}));
        assertEquals("Dia 1", 1,  calendario.diaDeLaSemana(new int[]{2019, 10, 14}));
        assertEquals("Dia 6", 6,  calendario.diaDeLaSemana(new int[]{2019, 10, 12}));
    }
}
