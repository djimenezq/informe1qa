package com.cenfotec.qagrupo5;

/**
 * Calidad y Verificación de Software - 3C 2019.
 * 11 - 10 - 2019
 * Universidad Cenfotec.
 * @author Roger Quiros Araya, Dereck y Sheng
 */

public class Calendario {

    /**
     * Metodo que permite determinar cual es la siguiente fecha en el calendario
     * dado una fecha de origen.
     * @param fecha - La fecha origin expresada como una tupla de 3 valores enteros positivos.
     * @exception Exception Lanza excepcion por fecha invalida
     * @return int[3]. La siguiente fecha expresada como un tupla de 3 valores enteros positivos. En caso de ser una fecha invalida retorna {0,0,0}
     */
    public int[] siguienteDia (int[] fecha) throws Exception {
        /*
         * Se corrobora que la fecha sea válida
         */
        if (!esFechaValida(fecha)) {
            throw new Exception("Fecha Invalida");
        }

        /*
         * En caso que el sumar más dias a la fecha supere la cantida de dias en el mes
         * Se debe incrementar el número de mes tambien
         */
        if ((fecha[2] + 1) > cantidadDiasEnUnMes(fecha[0], fecha[1])) {
            fecha[2] = 1;

            /*
             * En caso que incrementar la cantidad del mes supere la cantidad de meses en el annio
             * se debe incrementar el annio tambien.
             */
            if ((fecha[1] + 1) > 12) {
                fecha[1] = 1;
                fecha[0] += 1;
            } else {
                fecha[1]++;
            }
        } else {
            fecha[2]++;
        }

        return fecha;
    }

    /**
     * Permite saber cual dia de la semana corresponde una fecha ingresada
     * @param fecha - La fecha origin expresada como una tupla de 3 valores enteros positivos.
     * @exception Exception Lanza excepcion por fecha invalida
     * @return El numero de dia de la semana que correponde a la fecha. Comenzado por Domingo = 0 y acabando en Sabado = 6
     */
    public int diaDeLaSemana (int[] fecha) throws Exception {
        if (!esFechaValida(fecha)) {
            throw new Exception("Fecha Invalida");
        }

        /*
         * Cuando sacamos el modulo de 7 de la cantidad de dias transcurrido del annio nos da la el dia de la semana
         * comenzado por Lunes en 0, hay que convertirlo para Domingo sea el primer de la semana.
         */
        int diaDeLaSemana = diaDelAnnio(fecha) % 7;
        return (diaDeLaSemana + 1) >= 7 ? 0 : diaDeLaSemana + 1;
    }

    /**
     * Metodo que permite calcular el número día del año, con base a una fecha.
     *
     * Se realiza un sumatoria recursiva de todas las cantidades de dias de los meses anteriores
     * a el mes especificado originalmente en el argumento del metodo, restando en cada iteracion un mes.
     * En caso de no ser una fecha valida el metodo retornara ZERO
     *
     * @param fecha - La fecha origin expresada como una tupla de 3 valores enteros positivos.
     * @exception Exception Lanza excepcion por fecha invalida
     * @return Número intero con el número de día en el año indicado. Zero en caso no ser una fecha valida.
     */
    private int diaDelAnnio(int[] fecha) throws Exception {
        if (!esFechaValida(fecha)) {
            throw new Exception("Fecha Invalida");
        }

        fecha[1]--;
        while (fecha[1] >= 1) {
            fecha[2] += cantidadDiasEnUnMes(fecha[0], fecha[1]);
            fecha[1]--;
        }

        return fecha[2];
    }

    /**
     * Método que permite saber si un año es bisiesto o no.
     * @param annio - El número de año.
     * @return True en caso de ser bisciesto.
     */
    public boolean esBisiesto (int annio) {
        return annio % 4 == 0 && (annio % 100 != 0 || annio % 400 == 0);
    }

    /**
     * Método que permite saber si una fecha representa una fecha real.
     * @param fecha - La fecha origin expresada como una tupla de 3 valores enteros positivos.
     * @return True en caso de ser una fecha válida.
     */
    public boolean esFechaValida (int[] fecha) {
        return fecha.length == 3 && (fecha[2] >= 1 && fecha[2] <= cantidadDiasEnUnMes(fecha[0], fecha[1])) && (fecha[1] >= 1 && fecha[1] <= 12) && fecha[0] >= 1582;
    }

    /**
     * Método que permite saber cuantos días tiene un mes en un año determinado. Tomará en cuenta si el año es biciesto o no.
     * @param annio - El año al que pertenece el mes consultado.
     * @param mes - El número de mes a averiguar. Comienza en 1
     * @return Integer con la cantidad de dias en el mes.
     */
    private int cantidadDiasEnUnMes (int annio, int mes) {
        return (mes == 4 || mes == 6 || mes == 9 || mes == 11) ? 30 : mes == 2 ? (esBisiesto(annio) ? 29 : 28) : 31;
    }

}
